# News App
Get the most recent news in your hands by choosing the topic that you like. 

## Purpose
This app is not on Google Play Store and has been developed for learning and experience sharing purposes.  
It shows the use of **clean MVP architecture** to display story cards fetched from [Guardian API](https://open-platform.theguardian.com/documentation/) . 

## Features
The topic cards will be display in a RecyclerView. The details of each card will be show in a WebView to reduce the development scope.  
I included, user Preferences to change the cards order, the number of cards fetch on the server and a possibility for the user to customize the topic that he likes.

## Android Libraries used

This project show the use of some useful android libraries such as:

 -  **Retrofit** for networking.
 -  **RxJava** for reactive Stream.
 -  **Dagger 2.0** for dependency injection.
 -  **Robolectric** & **Mockito** for Unit tests
 -  **Espresso** for UI tests.
 - **Firebase** for notification.
 - **Gson** to easily transform POJO <-->JSON.
 
## Setup

### Clone the Repository

As usual, you get started by cloning the project to your local machine: 
```
$ git clone https://audreyange93@bitbucket.org/audreyteam/news-app.git
```
You can also create a new project from Android Studio by importing this project from git using the above git repository url.

### Prerequisites

This project uses JDK 8  and  has been build using android SDK 28. It also use the [androidx.*](https://developer.android.com/jetpack/androidx) package libraries in the latest version.

### Screenshots
![screenshot_gif](https://drive.google.com/uc?export=download&id=1u-entVS_PkxeRSBliGApKJ3k6Bpuf6ZZ)

## Screencast
Here a video showing a demo of the app :
[News App Video](https://drive.google.com/uc?export=download&id=1PhZ7fa1IGNd66nPRMs_lFKv8IhMdAxc2).

## Testing

This project include  tests :

 - Local Unit Test with JUnit (faster than test using Robolectric)
 - Android Unit Test  with Robolectric
 - Android UI Test with Espresso

## Authors

 - **Audrey Maumoin**    
 Android Developer


## Acknowledgments
I hope that you will have a good time navigating through the project.   
Feel free to comment or ask if you want to know more or have some suggestions.  
**Happy Coding :thumbsup:**