package com.example.android.newsapp;

import android.app.Activity;
import android.app.Application;
import com.example.android.newsapp.di.AppModule;
import com.example.android.newsapp.di.DaggerNewsComponent;
import com.example.android.newsapp.di.NetworkModule;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import javax.inject.Inject;

public class NewsApplication extends Application implements HasActivityInjector {

  @Inject
  DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

  @Override
  public void onCreate() {
    super.onCreate();
    DaggerNewsComponent.builder().appModule(new AppModule(this))
        .networkModule(new NetworkModule(Config.BASE_URL)).build().inject(this);
  }

  @Override
  public AndroidInjector<Activity> activityInjector() {
    return dispatchingActivityInjector;
  }
}
