package com.example.android.newsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.newsapp.R;
import com.example.android.newsapp.di.annotation.Scope.ActivityScope;
import com.example.android.newsapp.fragment.StoryFragment;
import com.example.android.newsapp.fragment.StoryFragment.StoryFragmentModule;
import com.example.android.newsapp.utils.PrefsUtils;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import java.util.Set;
import javax.inject.Inject;

public class MainActivity extends BaseActivity {

  public static final String EXTRA_SHOULD_NOT_REFRESH_CONTENT = "action_should_not_refresh_content";
  private static final String NAV_LIST_VIEW_STATE_KEY = "nav_list_view_state_key";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.nav_drawer_list)
  ListView navDrawerListView;
  @BindView(R.id.drawer_layout)
  DrawerLayout drawerLayout;
  @Inject
  PrefsUtils prefsUtils;

  private StoryFragment storyFragment;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    ActionBar actionbar = getSupportActionBar();
    // Put the nav drawer icon in the action bar
    if (actionbar != null) {
      actionbar.setDisplayHomeAsUpEnabled(true);
      actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    loadStoryFragment(savedInstanceState);
    // Retrieves the categories selected by the user to initialize the listView
    // in the nav drawer.
    Set<String> categorySelectedNames = prefsUtils.getSelectedCategories();
    String[] array = categorySelectedNames.toArray(new String[categorySelectedNames.size()]);
    final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
        android.R.layout.simple_list_item_activated_1, array);
    navDrawerListView.setAdapter(adapter);
    navDrawerListView.setOnItemClickListener((parent, view, position, id) -> {
      drawerLayout.closeDrawer(GravityCompat.START);
      String title = parent.getItemAtPosition(position).toString();
      setTitle(title);
      storyFragment.notifySectionNameChanged();
    });

    if (savedInstanceState == null) {
      // Checks the first item in the listView when the activity is first launched
      navDrawerListView.setItemChecked(0, true);
    } else {
      // Retrieves the listView instance state and restore it
      Parcelable listViewInstanceState = savedInstanceState.getParcelable(NAV_LIST_VIEW_STATE_KEY);
      navDrawerListView.onRestoreInstanceState(listViewInstanceState);
    }
    // Sets the activity title depending on the item checked in the listView.
    int checkedPosition = navDrawerListView.getCheckedItemPosition();
    String title = (String) navDrawerListView.getAdapter().getItem(checkedPosition);
    setTitle(title);
  }

  private void loadStoryFragment(Bundle savedInstanceState) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    if (savedInstanceState == null) {
      storyFragment = new StoryFragment();
      transaction.replace(R.id.frame_layout, storyFragment).commit();
    } else {
      storyFragment = (StoryFragment) getSupportFragmentManager()
          .findFragmentById(R.id.frame_layout);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putParcelable(NAV_LIST_VIEW_STATE_KEY, navDrawerListView.onSaveInstanceState());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.settings_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        drawerLayout.openDrawer(GravityCompat.START);
        return true;
      case R.id.menu_settings:
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    // Handles back click to close the nav drawer.
    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
      drawerLayout.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    if (!intent.hasExtra(EXTRA_SHOULD_NOT_REFRESH_CONTENT)) {
      storyFragment.onRefresh();
    }
  }

  @ActivityScope
  @Subcomponent(modules = StoryFragmentModule.class)
  public interface MainActivitySubComponent extends AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {

    }
  }
}
