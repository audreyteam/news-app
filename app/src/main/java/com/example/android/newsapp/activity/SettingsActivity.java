package com.example.android.newsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import com.example.android.newsapp.R;

public class SettingsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.settings_activity);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    setTitle(R.string.settings);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    navigateToParentActivity();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        navigateToParentActivity();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void navigateToParentActivity() {
    Intent upIntent = NavUtils.getParentActivityIntent(this);
    upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    NavUtils.navigateUpTo(this, upIntent);
  }

  public static class StoryPreferenceFragment extends PreferenceFragment implements
      OnPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.preference);

      Preference orderBy = findPreference(getString(R.string.settings_order_by_key));
      bindPreference(orderBy);
      Preference pageSize = findPreference(getString(R.string.settings_page_size_key));
      bindPreference(pageSize);
      Preference category = findPreference(getString(R.string.settings_interest_key));
      category.setOnPreferenceClickListener(preference -> {
        Intent intent = new Intent(getActivity(), StoryCategoryActivity.class);
        startActivity(intent);
        return true;
      });
    }

    private void bindPreference(Preference preference) {
      preference.setOnPreferenceChangeListener(this);
      SharedPreferences preferences = PreferenceManager
          .getDefaultSharedPreferences(preference.getContext());
      String preferenceString = preferences.getString(preference.getKey(), "");
      onPreferenceChange(preference, preferenceString);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
      String stringValue = newValue.toString();
      if (preference instanceof ListPreference) {
        ListPreference listPreference = (ListPreference) preference;
        int prefIndex = listPreference.findIndexOfValue(stringValue);
        if (prefIndex >= 0) {
          CharSequence[] labels = listPreference.getEntries();
          preference.setSummary(labels[prefIndex]);
        }
      }
      return true;
    }
  }
}
