package com.example.android.newsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import com.example.android.newsapp.R;
import com.example.android.newsapp.di.annotation.Scope.ActivityScope;
import com.example.android.newsapp.fragment.StartFragment;
import com.example.android.newsapp.fragment.StartFragment.StartFragmentModule;
import com.example.android.newsapp.utils.PrefsUtils;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import javax.inject.Inject;

public class StartActivity extends BaseActivity {

  @Inject
  PrefsUtils prefsUtils;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    boolean isFirstTime = prefsUtils.isFirstTimeUser();
    if (isFirstTime) {
      setContentView(R.layout.fragment_container);
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      transaction.replace(R.id.frame_layout, new StartFragment()).commit();
    } else {
      Intent intent = new Intent(this, MainActivity.class);
      startActivity(intent);
      finish();
    }
  }

  @ActivityScope
  @Subcomponent(modules = StartFragmentModule.class)
  public interface StartActivitySubComponent extends AndroidInjector<StartActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StartActivity> {

    }
  }
}
