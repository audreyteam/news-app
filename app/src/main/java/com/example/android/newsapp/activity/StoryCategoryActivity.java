package com.example.android.newsapp.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import com.example.android.newsapp.R;
import com.example.android.newsapp.di.annotation.Scope.ActivityScope;
import com.example.android.newsapp.fragment.StoryCategoryFragment;
import com.example.android.newsapp.fragment.StoryCategoryFragment.StoryCategoryFragmentModule;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

public class StoryCategoryActivity extends BaseActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_container);
    setTitle(R.string.category_activity_title);
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.replace(R.id.frame_layout, new StoryCategoryFragment()).commit();
  }

  @ActivityScope
  @Subcomponent(modules = StoryCategoryFragmentModule.class)
  public interface StoryCategoryActivitySubComponent extends
      AndroidInjector<StoryCategoryActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StoryCategoryActivity> {

    }
  }
}
