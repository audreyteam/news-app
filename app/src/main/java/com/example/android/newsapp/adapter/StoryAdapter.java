package com.example.android.newsapp.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.example.android.newsapp.R;
import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.model.Story.TagEditor;
import com.example.android.newsapp.utils.DateUtils;
import java.util.List;

public class StoryAdapter extends Adapter<ViewHolder> {

  private static final int WIDE_CARD_VIEW_TYPE = 0;
  private static final int SMALL_CARD_VIEW_TYPE = 1;
  private static final int LOAD_MORE_CARD_VIEW_TYPE = 2;

  private Context context;
  private List<Story> storyList;
  private OnStoryItemClickListener listener;

  public StoryAdapter(Context context, List<Story> storyList) {
    this.context = context;
    this.storyList = storyList;
  }

  public void clearList() {
    storyList.clear();
    notifyDataSetChanged();
  }

  public void addList(List<Story> stories) {
    storyList.addAll(stories);
    notifyDataSetChanged();
  }

  public void setOnStoryItemClick(OnStoryItemClickListener listener) {
    this.listener = listener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view;
    ViewHolder viewHolder;
    switch (viewType) {
      case LOAD_MORE_CARD_VIEW_TYPE:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.load_more_card, parent, false);
        viewHolder = new LoadMoreViewHolder(view);
        break;
      case WIDE_CARD_VIEW_TYPE:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.story_item_wide_card, parent, false);
        viewHolder = new StoryCardViewHolder(view);
        break;
      case SMALL_CARD_VIEW_TYPE:
      default:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.story_item_small_card, parent, false);
        viewHolder = new StoryCardViewHolder(view);
        break;
    }
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
    int viewType = getItemViewType(position);
    switch (viewType) {
      case WIDE_CARD_VIEW_TYPE:
      case SMALL_CARD_VIEW_TYPE:
        StoryCardViewHolder holder = (StoryCardViewHolder) viewHolder;
        final Story currentStory = storyList.get(position);
        holder.sectionNameView.setText(currentStory.getSectionName());
        holder.titleView.setText(currentStory.getTitle());
        setPublicationDateView(holder.dateView, currentStory.getPublicationDate());
        if (!TextUtils.isEmpty(currentStory.getDescription())) {
          holder.descriptionView.setText(Html.fromHtml(currentStory.getDescription()));
        }
        if (TextUtils.isEmpty(currentStory.getImageUrl())) {
          holder.imageView.setVisibility(View.GONE);
        } else {
          holder.imageView.setVisibility(View.VISIBLE);
          Glide.with(context)
              .load(currentStory.getImageUrl())
              .into(holder.imageView);
        }
        final TagEditor currentEditor = currentStory.getTagEditor();
        if (currentEditor != null) {
          if (TextUtils.isEmpty(currentEditor.getEditorName())) {
            holder.editorImageView.setVisibility(View.GONE);
          } else {
            holder.editorImageView.setVisibility(View.VISIBLE);
          }
          holder.editorNameView.setText(currentEditor.getEditorName());
          String editorImageUrl = currentEditor.getEditorImageUrl();
          if (!TextUtils.isEmpty(editorImageUrl)) {
            Glide.with(context)
                .load(currentEditor.getEditorImageUrl())
                .into(holder.editorImageView);
          }
        }
        holder.cardView.setOnClickListener(view -> listener.onStoryItemClick(currentStory));
        holder.editorNameView.setOnClickListener(view -> listener.onEditorClick(currentEditor));
        break;
      case LOAD_MORE_CARD_VIEW_TYPE:
        LoadMoreViewHolder loadMoreHolder = (LoadMoreViewHolder) viewHolder;
        loadMoreHolder.cardView.setOnClickListener(v -> listener.onLoadMoreStories());
        break;
    }
  }

  /**
   * Sets the publication date in the TextView
   */
  private void setPublicationDateView(TextView view, String publicationDate) {
    long elapsedTimeInMs =
        System.currentTimeMillis() - DateUtils.convertDateToMs(
            publicationDate);
    view.setText(DateUtils.toReadableTime(context, elapsedTimeInMs));
  }

  @Override
  public int getItemCount() {
    int size = storyList.size();
    // Because of the load more button, we need to return the size + 1 when there are data in the list.
    return size > 0 ? size + 1 : size;
  }

  @Override
  public int getItemViewType(int position) {
    if (position == storyList.size()) {
      return LOAD_MORE_CARD_VIEW_TYPE;
    } else if (position % 2 == 0) {
      return WIDE_CARD_VIEW_TYPE;
    } else {
      return SMALL_CARD_VIEW_TYPE;
    }
  }

  /** Listener that handle click events on story item card. */
  public interface OnStoryItemClickListener {

    void onStoryItemClick(Story story);

    void onEditorClick(TagEditor editor);

    void onLoadMoreStories();
  }

  /** ViewHolder for that present a StoryCard. */
  static class StoryCardViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.section_name)
    TextView sectionNameView;
    @BindView(R.id.news_image)
    ImageView imageView;
    @BindView(R.id.news_title)
    TextView titleView;
    @BindView(R.id.news_description)
    TextView descriptionView;
    @BindView(R.id.editor_image)
    ImageView editorImageView;
    @BindView(R.id.editor)
    TextView editorNameView;
    @BindView(R.id.date)
    TextView dateView;
    @BindView(R.id.card)
    CardView cardView;

    StoryCardViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

  /** ViewHolder that present the Load more button. */
  static class LoadMoreViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card)
    CardView cardView;

    LoadMoreViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}

