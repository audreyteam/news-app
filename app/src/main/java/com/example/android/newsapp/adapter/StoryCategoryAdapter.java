package com.example.android.newsapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.newsapp.R;
import com.example.android.newsapp.adapter.StoryCategoryAdapter.StoryCategoryViewHolder;
import com.example.android.newsapp.model.StoryCategory;
import java.util.List;

public class StoryCategoryAdapter extends Adapter<StoryCategoryViewHolder> {

  private Context context;
  private List<StoryCategory> categoryList;

  public StoryCategoryAdapter(Context context, List<StoryCategory> categoryList) {
    this.context = context;
    this.categoryList = categoryList;
  }

  @NonNull
  @Override
  public StoryCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.story_category_item, parent, false);
    StoryCategoryViewHolder viewHolder = new StoryCategoryViewHolder(view);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull final StoryCategoryViewHolder holder, int position) {
    final StoryCategory category = categoryList.get(position);
    holder.categoryName.setText(category.getName());
    holder.categoryImage.setImageResource(category.getImageResourceId());
    setSelectedViewVisibility(holder.selectedView,
        category.isChecked() || category.isSelectedByDefault());
    // Sets the color depending on the category selected by the user or by default.
    int selectedColor = context.getResources().getColor(
        category.isSelectedByDefault() ? R.color.defaultSelectedBackground
            : R.color.checkedStateBackground);
    holder.selectedView.setBackgroundColor(selectedColor);
    holder.categoryContainer.setOnClickListener(v -> {
      if (category.isSelectedByDefault()) {
        return;
      }
      category.setChecked(!category.isChecked());
      setSelectedViewVisibility(holder.selectedView, category.isChecked());
    });
  }

  /**
   * Sets the selected view visibility based on the state.
   */
  private void setSelectedViewVisibility(View selectedView, boolean state) {
    selectedView.setVisibility(state ? View.VISIBLE : View.GONE);
  }

  @Override
  public int getItemCount() {
    return categoryList.size();
  }

  static class StoryCategoryViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.category_image)
    ImageView categoryImage;
    @BindView(R.id.category_name)
    TextView categoryName;
    @BindView(R.id.category_container)
    LinearLayout categoryContainer;
    @BindView(R.id.selected_view)
    LinearLayout selectedView;

    StoryCategoryViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
