package com.example.android.newsapp.di;

import android.app.Application;
import android.content.Context;
import com.example.android.newsapp.di.annotation.ApplicationContext;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class AppModule {

  private Application application;

  public AppModule(Application application) {
    this.application = application;
  }

  @Provides
  @Singleton
  @ApplicationContext
  public Context provideApplicationContext() {
    return application;
  }
}
