package com.example.android.newsapp.di;

import com.example.android.newsapp.activity.MainActivity;
import com.example.android.newsapp.activity.MainActivity.MainActivitySubComponent;
import com.example.android.newsapp.activity.StartActivity;
import com.example.android.newsapp.activity.StartActivity.StartActivitySubComponent;
import com.example.android.newsapp.activity.StoryCategoryActivity;
import com.example.android.newsapp.activity.StoryCategoryActivity.StoryCategoryActivitySubComponent;
import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {StartActivitySubComponent.class, MainActivitySubComponent.class,
    StoryCategoryActivitySubComponent.class})
public abstract class ApplicationBinders {

  @Binds
  @IntoMap
  @ClassKey(StartActivity.class)
  public abstract AndroidInjector.Factory<?>
  bindStartActivityInjector(StartActivitySubComponent.Builder builder);

  @Binds
  @IntoMap
  @ClassKey(MainActivity.class)
  public abstract AndroidInjector.Factory<?>
  bindMainActivityInjector(MainActivitySubComponent.Builder builder);

  @Binds
  @IntoMap
  @ClassKey(StoryCategoryActivity.class)
  public abstract AndroidInjector.Factory<?>
  bindStoryCategoryActivityInjector(StoryCategoryActivitySubComponent.Builder builder);
}
