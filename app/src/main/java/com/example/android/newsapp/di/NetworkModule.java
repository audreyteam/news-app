package com.example.android.newsapp.di;

import com.example.android.newsapp.repo.rest.NetworkInterceptor;
import com.example.android.newsapp.repo.rest.RestClient;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

  private static final int READ_TIME_OUT = 10000;
  private static final int CONNECT_TIME_OUT = 15000;

  private String baseUrl;

  public NetworkModule(String url) {
    this.baseUrl = url;
  }

  @Provides
  @Singleton
  public HttpLoggingInterceptor provideInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(Level.BASIC);
    return interceptor;
  }

  @Provides
  @Singleton
  public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor, NetworkInterceptor networkInterceptor) {
    OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
        .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(READ_TIME_OUT, TimeUnit.SECONDS);
    httpClient.addInterceptor(interceptor);
    httpClient.addInterceptor(networkInterceptor);
    return httpClient.build();
  }

  @Provides
  @Singleton
  public Retrofit provideRetrofit(OkHttpClient httpClient) {
    return new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            GsonConverterFactory.create())
        .client(httpClient)
        .build();
  }

  @Provides
  @Singleton
  public RestClient provideRestClient(Retrofit retrofit) {
    return new RestClient(retrofit);
  }

}
