package com.example.android.newsapp.di;

import com.example.android.newsapp.NewsApplication;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import javax.inject.Singleton;

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationBinders.class, AppModule.class,
    NetworkModule.class})
public interface NewsComponent extends AndroidInjector<NewsApplication> {

}
