package com.example.android.newsapp.di.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Scope {

  @javax.inject.Scope
  @Documented
  @Retention(value = RetentionPolicy.RUNTIME)
  public @interface FragmentScope {

  }

  @javax.inject.Scope
  @Documented
  @Retention(value = RetentionPolicy.RUNTIME)
  public @interface ActivityScope {

  }
}
