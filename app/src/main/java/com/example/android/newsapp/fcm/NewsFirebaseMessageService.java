package com.example.android.newsapp.fcm;

import static com.example.android.newsapp.activity.MainActivity.EXTRA_SHOULD_NOT_REFRESH_CONTENT;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.example.android.newsapp.R;
import com.example.android.newsapp.activity.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NewsFirebaseMessageService extends FirebaseMessagingService {

  private static final String TAG = "NewsFirebase";
  private static final String NOTIFICATION_CHANNEL_ID = "channel_id";
  private static final int NOTIFICATION_ID = 0;
  private static final int PENDING_INTENT_REQUEST_CODE = 0;

  @Override
  public void onNewToken(String token) {
    super.onNewToken(token);
    Log.d(TAG, "token updated ->" + token);
  }

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
    sendNotification(remoteMessage);
  }

  private void sendNotification(RemoteMessage remoteMessage) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    intent.putExtra(EXTRA_SHOULD_NOT_REFRESH_CONTENT, true);
    // Create the pending intent to launch the activity
    PendingIntent pendingIntent = PendingIntent
        .getActivity(this, PENDING_INTENT_REQUEST_CODE, intent,
            PendingIntent.FLAG_ONE_SHOT);

    if (remoteMessage.getNotification() != null) {
      Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
      NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
          NOTIFICATION_CHANNEL_ID)
          .setContentTitle(remoteMessage.getNotification().getTitle())
          .setContentText(remoteMessage.getNotification().getBody())
          .setSmallIcon(R.drawable.ic_launcher_background)
          .setAutoCancel(true)
          .setSound(defaultSoundUri)
          .setContentIntent(pendingIntent);

      NotificationManager notificationManager =
          (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }
  }
}
