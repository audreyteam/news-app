package com.example.android.newsapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.example.android.newsapp.R;
import com.example.android.newsapp.activity.StoryCategoryActivity;
import com.example.android.newsapp.di.annotation.Scope.FragmentScope;
import com.example.android.newsapp.utils.PrefsUtils;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import java.util.Arrays;
import java.util.HashSet;
import javax.inject.Inject;

/** Fragment that display a welcome page */
public class StartFragment extends Fragment {

  static final String FROM_START_FRAGMENT_KEY = "is_opened_from_start_fragment";

  @BindView(R.id.start_button)
  Button startButton;
  @Inject
  PrefsUtils prefsUtils;
  private Unbinder unbinder;

  @Override
  public void onAttach(@NonNull Context context) {
    AndroidSupportInjection.inject(this);
    super.onAttach(context);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.get_started_layout, container, false);
    unbinder = ButterKnife.bind(this, rootView);
    return rootView;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @OnClick(R.id.start_button)
  public void onStartButtonClicked() {
    // Saves the default selected categories as init states.
    String[] defaultCategoriesSelected = getResources()
        .getStringArray(R.array.story_category_names_selected_by_default);
    prefsUtils.putSelectedCategories(new HashSet<>(Arrays.asList(defaultCategoriesSelected)));

    Intent intent = new Intent(getActivity(), StoryCategoryActivity.class);
    intent.putExtra(FROM_START_FRAGMENT_KEY, true);
    startActivity(intent);
    // Puts the user first time to false.
    prefsUtils.putFirstTimeUser();
    getActivity().finish();
  }

  @FragmentScope
  @Subcomponent
  public interface StartFragmentSubComponent extends AndroidInjector<StartFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StartFragment> {

    }
  }

  @Module(subcomponents = StartFragmentSubComponent.class)
  public abstract static class StartFragmentModule {

    @Binds
    @IntoMap
    @ClassKey(StartFragment.class)
    public abstract AndroidInjector.Factory<?>
    bindStartFragmentInjector(StartFragmentSubComponent.Builder builder);
  }
}
