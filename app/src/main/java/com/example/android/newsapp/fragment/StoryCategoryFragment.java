package com.example.android.newsapp.fragment;

import static com.example.android.newsapp.fragment.StartFragment.FROM_START_FRAGMENT_KEY;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.example.android.newsapp.R;
import com.example.android.newsapp.activity.MainActivity;
import com.example.android.newsapp.adapter.StoryCategoryAdapter;
import com.example.android.newsapp.di.annotation.Scope.FragmentScope;
import com.example.android.newsapp.model.StoryCategory;
import com.example.android.newsapp.utils.PrefsUtils;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

/** Fragment that display a list of selectable topic categories */
public class StoryCategoryFragment extends Fragment {

  private static final int COLUMN_COUNT = 3;
  private static final int NO_RES_ID = -1;

  @BindView(R.id.category_recycler_view)
  RecyclerView categoryRecyclerView;
  @BindView(R.id.continue_button)
  Button continueButton;
  @Inject
  PrefsUtils prefsUtils;
  private Unbinder unbinder;
  private List<StoryCategory> categoryList;

  @Override
  public void onAttach(@NonNull Context context) {
    AndroidSupportInjection.inject(this);
    super.onAttach(context);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.story_category_list, container, false);
    unbinder = ButterKnife.bind(this, rootView);

    categoryList = getStoryCategoryList();
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), COLUMN_COUNT);
    categoryRecyclerView.setLayoutManager(layoutManager);
    StoryCategoryAdapter adapter = new StoryCategoryAdapter(getActivity(), categoryList);
    categoryRecyclerView.setAdapter(adapter);
    return rootView;
  }

  private List<StoryCategory> getStoryCategoryList() {
    List<StoryCategory> categoryList = new ArrayList<>();
    Resources res = getResources();
    String[] storyCategoryNameArray = res.getStringArray(R.array.story_category_names);
    String[] defaultSelectedArray = res
        .getStringArray(R.array.story_category_names_selected_by_default);
    List<String> defaultSelectedList = Arrays.asList(defaultSelectedArray);
    TypedArray imageArray = res.obtainTypedArray(R.array.story_category_image_array);
    Set<String> categoryNames = prefsUtils.getSelectedCategories();
    for (int i = 0; i < storyCategoryNameArray.length; i++) {
      String name = storyCategoryNameArray[i];
      boolean hasNameSelected = categoryNames.contains(name);
      boolean isSelectedByDefault = defaultSelectedList.contains(name);
      StoryCategory category = new StoryCategory(name, imageArray.getResourceId(i, NO_RES_ID),
          hasNameSelected);
      category.setSelectedByDefault(isSelectedByDefault);
      categoryList.add(category);
    }
    imageArray.recycle();
    return categoryList;
  }

  private boolean isFromStartFragment() {
    Intent intent = getActivity().getIntent();
    if (intent != null) {
      return intent.getBooleanExtra(FROM_START_FRAGMENT_KEY, false);
    }
    return false;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.category_activity_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_close:
        if (isFromStartFragment()) {
          navigateToNews();
        } else {
          getActivity().finish();
        }
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @OnClick(R.id.continue_button)
  public void onViewClicked() {
    Set<String> categorySelectedNames = new HashSet<>();
    for (StoryCategory category : categoryList) {
      if (category.isChecked() || category.isSelectedByDefault()) {
        categorySelectedNames.add(category.getName());
      }
    }
    prefsUtils.putSelectedCategories(categorySelectedNames);
    navigateToNews();
  }

  private void navigateToNews() {
    Intent intent = new Intent(getActivity(), MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
    getActivity().finish();
  }

  @FragmentScope
  @Subcomponent
  public interface StoryCategoryFragmentSubComponent extends
      AndroidInjector<StoryCategoryFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StoryCategoryFragment> {

    }
  }

  @Module(subcomponents = StoryCategoryFragmentSubComponent.class)
  public abstract static class StoryCategoryFragmentModule {

    @Binds
    @IntoMap
    @ClassKey(StoryCategoryFragment.class)
    public abstract AndroidInjector.Factory<?>
    bindStoryCategoryFragmentInjector(StoryCategoryFragmentSubComponent.Builder builder);
  }
}
