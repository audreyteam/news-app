package com.example.android.newsapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.android.newsapp.R;
import com.example.android.newsapp.adapter.StoryAdapter;
import com.example.android.newsapp.adapter.StoryAdapter.OnStoryItemClickListener;
import com.example.android.newsapp.di.annotation.Scope.FragmentScope;
import com.example.android.newsapp.fragment.StoryPresenter.StoryView;
import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.model.Story.TagEditor;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/** Fragment that display a list of cards based on a topic chosen. */
public class StoryFragment extends Fragment implements OnRefreshListener, OnStoryItemClickListener,
    StoryView {

  private static final int INITIAL_POSITION = 0;

  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;
  @BindView(R.id.empty_view)
  TextView emptyView;
  @BindView(R.id.swipe_refresh_layout)
  SwipeRefreshLayout refreshLayout;
  @Inject
  StoryPresenter storyPresenter;
  private Unbinder unbinder;
  private StoryAdapter adapter;

  @Override
  public void onAttach(@NonNull Context context) {
    AndroidSupportInjection.inject(this);
    super.onAttach(context);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View rootView = inflater.inflate(R.layout.fragment_main, container, false);
    unbinder = ButterKnife.bind(this, rootView);

    refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    refreshLayout.setOnRefreshListener(this);
    refreshLayout.setRefreshing(true);

    adapter = new StoryAdapter(getActivity(), new ArrayList<Story>());
    adapter.setOnStoryItemClick(this);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
        RecyclerView.VERTICAL, false);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);
    storyPresenter.fetchStories(getSectionTitle());
    return rootView;
  }

  private String getSectionTitle() {
    return getActivity().getTitle().toString();
  }

  @Override
  public void onRefresh() {
    storyPresenter.refreshStories(getSectionTitle());
  }

  @Override
  public void scrollStoriesToBeginning() {
    recyclerView.scrollToPosition(INITIAL_POSITION);
  }

  @Override
  public void showNoConnectionToast() {
    Toast.makeText(getActivity(), R.string.no_connection, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onStoryItemClick(Story story) {
    storyPresenter.launchWebsite(story.getWebUrl());
  }

  @Override
  public void onEditorClick(TagEditor editor) {
    storyPresenter.launchWebsite(editor.getEditorWebUrl());
  }

  @Override
  public void onLoadMoreStories() {
    storyPresenter.loadMoreStories(getSectionTitle());
  }

  @Override
  public void launchWebsite(String webUrl) {
    Uri website = Uri.parse(webUrl);
    Intent intent = new Intent(Intent.ACTION_VIEW, website);
    if (intent.resolveActivity((getActivity().getPackageManager())) != null) {
      startActivity(intent);
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    storyPresenter.onDestroy();
    unbinder.unbind();
  }

  public void notifySectionNameChanged() {
    storyPresenter.changeSectionName(getSectionTitle());
  }

  @Override
  public void displayNews(List<Story> storyList, boolean isLoadingMore) {
    if (!isLoadingMore) {
      adapter.clearList();
    }
    if (storyList != null && !storyList.isEmpty()) {
      adapter.addList(storyList);
      setEmptyViewVisibility(false);
    } else {
      setEmptyViewVisibility(true);
      emptyView.setText(R.string.no_data);
    }
  }

  private void setEmptyViewVisibility(boolean isVisible) {
    if (isVisible) {
      recyclerView.setVisibility(View.GONE);
      emptyView.setVisibility(View.VISIBLE);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      emptyView.setVisibility(View.GONE);
    }
  }

  @Override
  public void showNoConnectionError() {
    setEmptyViewVisibility(true);
    emptyView.setText(R.string.no_connection);
  }

  @Override
  public void setRefreshingState(boolean state) {
    refreshLayout.setRefreshing(state);
  }

  @FragmentScope
  @Subcomponent(modules = PresenterProviderModule.class)
  public interface StoryFragmentSubComponent extends AndroidInjector<StoryFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StoryFragment> {

    }
  }

  @Module(subcomponents = StoryFragmentSubComponent.class)
  public abstract static class StoryFragmentModule {

    @Binds
    @IntoMap
    @ClassKey(StoryFragment.class)
    public abstract AndroidInjector.Factory<?>
    bindStoryFragmentInjector(StoryFragmentSubComponent.Builder builder);
  }

  @Module
  public static class PresenterProviderModule {

    @Provides
    @FragmentScope
    public StoryView provideStoryView(StoryFragment fragment) {
      return fragment;
    }
  }
}
