package com.example.android.newsapp.fragment;

import android.util.Log;
import androidx.annotation.VisibleForTesting;
import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.repo.StoryRepository;
import com.example.android.newsapp.repo.exception.ConnectivityException;
import com.example.android.newsapp.utils.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

/**
 * Presenter use to present {@link StoryFragment} logic.
 */
public class StoryPresenter {

  private static final int PAGE_START = 1;

  private final CompositeDisposable compositeDisposable;
  private final SchedulerProvider schedulerProvider;
  private final StoryRepository storyRepository;
  private StoryView storyView;
  private int pageToLoad = PAGE_START;

  private final Consumer<Throwable> onLoadError = (Throwable throwable) -> {
    storyView.setRefreshingState(false);
    Log.e(this.toString(), "error happens ->" + throwable.getLocalizedMessage());
    if (throwable instanceof ConnectivityException) {
      storyView.showNoConnectionError();
    } else {
      storyView.displayNews(Collections.emptyList(), false);
    }
  };

  private final Consumer<Throwable> onRefreshError = (Throwable throwable) -> {
    Log.e(this.toString(), "error happens ->" + throwable.getLocalizedMessage());
    storyView.setRefreshingState(false);
    storyView.showNoConnectionToast();
  };

  @Inject
  public StoryPresenter(StoryView storyView, StoryRepository storyRepository,
      SchedulerProvider schedulerProvider) {
    this.storyView = storyView;
    this.schedulerProvider = schedulerProvider;
    this.storyRepository = storyRepository;
    compositeDisposable = new CompositeDisposable();
  }

  @VisibleForTesting
  CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }

  void refreshStories(String sectionName) {
    pageToLoad = PAGE_START;
    startFetchStories(sectionName, FetchType.REFRESH);
  }

  void fetchStories(String sectionName) {
    startFetchStories(sectionName, FetchType.DEFAULT);
  }

  private void startFetchStories(String sectionName, FetchType fetchType) {
    storyView.setRefreshingState(true);
    compositeDisposable
        .add(storyRepository
            .getStoryList(getSectionValue(sectionName), String.valueOf(pageToLoad))
            .subscribeOn(schedulerProvider.io())
            .observeOn(
                schedulerProvider.mainThread())
            .subscribe(storyList -> {
              if (fetchType == FetchType.REFRESH) {
                storyView.scrollStoriesToBeginning();
              }
              storyView.setRefreshingState(false);
              storyView.displayNews(storyList, fetchType == FetchType.LOAD_MORE);
            }, shouldShowToastError(fetchType) ? onRefreshError : onLoadError));
  }

  private boolean shouldShowToastError(FetchType fetchType) {
    return fetchType == FetchType.REFRESH || fetchType == FetchType.LOAD_MORE;
  }

  private String getSectionValue(String sectionName) {
    return sectionName.replace(" ", "").toLowerCase();
  }

  void loadMoreStories(String sectionName) {
    pageToLoad++;
    startFetchStories(sectionName, FetchType.LOAD_MORE);
  }

  void changeSectionName(String sectionName) {
    pageToLoad = PAGE_START;
    storyView.scrollStoriesToBeginning();
    startFetchStories(sectionName, FetchType.UPDATE_SECTION);
  }

  void launchWebsite(String url) {
    storyView.launchWebsite(url);
  }

  void onDestroy() {
    compositeDisposable.clear();
    storyView = null;
  }

  enum FetchType {
    REFRESH,
    LOAD_MORE,
    UPDATE_SECTION,
    DEFAULT
  }

  public interface StoryView {

    void displayNews(List<Story> stories, boolean isLoadingMore);

    void showNoConnectionError();

    void setRefreshingState(boolean state);

    void launchWebsite(String webUrl);

    void showNoConnectionToast();

    void scrollStoriesToBeginning();
  }
}
