package com.example.android.newsapp.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Story {

  private String sectionName;
  @SerializedName("webTitle")
  private String title;
  private String webUrl;
  @SerializedName("tags")
  private List<TagEditor> tagEditors;
  @SerializedName("fields")
  private Field field;
  @SerializedName("webPublicationDate")
  private String publicationDate;

  public Story(String sectionName, String title, String webUrl) {
    this.sectionName = sectionName;
    this.title = title;
    this.webUrl = webUrl;
  }

  public String getSectionName() {
    return sectionName;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return field != null ? field.description : null;
  }

  public String getWebUrl() {
    return webUrl;
  }

  public TagEditor getTagEditor() {
    return tagEditors != null && !tagEditors.isEmpty() ? tagEditors.get(0) : null;
  }

  public String getImageUrl() {
    return field != null ? field.imageUrl : null;
  }

  public String getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(String publicationDate) {
    this.publicationDate = publicationDate;
  }

  public static class TagEditor {

    @SerializedName("webTitle")
    private String editorName;
    @SerializedName("bylineImageUrl")
    private String editorImageUrl;
    @SerializedName("webUrl")
    private String editorWebUrl;

    public TagEditor(String editorName, String editorWebUrl, String editorImageUrl) {
      this.editorName = editorName;
      this.editorImageUrl = editorImageUrl;
      this.editorWebUrl = editorWebUrl;
    }

    public String getEditorName() {
      return editorName;
    }

    public String getEditorImageUrl() {
      return editorImageUrl;
    }

    public String getEditorWebUrl() {
      return editorWebUrl;
    }
  }

  private static class Field {

    @SerializedName("trailText")
    private String description;
    @SerializedName("thumbnail")
    private String imageUrl;

  }
}
