package com.example.android.newsapp.model;

public class StoryCategory {

  private String name;
  private int imageResourceId;
  private boolean isChecked;
  private boolean isSelectedByDefault;

  public StoryCategory(String name, int imageResourceId, boolean isChecked) {
    this.name = name;
    this.imageResourceId = imageResourceId;
    this.isChecked = isChecked;
  }

  public String getName() {
    return name;
  }

  public int getImageResourceId() {
    return imageResourceId;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }

  public boolean isSelectedByDefault() {
    return isSelectedByDefault;
  }

  public void setSelectedByDefault(boolean selectedByDefault) {
    isSelectedByDefault = selectedByDefault;
  }
}
