package com.example.android.newsapp.repo;

import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.repo.rest.RestClient;
import com.example.android.newsapp.repo.rest.StoryApi;
import com.example.android.newsapp.utils.PrefsUtils;
import io.reactivex.Single;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public class StoryRepository {

  private final PrefsUtils prefsUtils;
  private final RestClient restClient;

  @Inject
  public StoryRepository(RestClient restClient, PrefsUtils prefsUtils) {
    this.restClient = restClient;
    this.prefsUtils = prefsUtils;
  }

  public Single<List<Story>> getStoryList(String section, String pageValue) {
      StoryApi storyApi = restClient.getApiClient(StoryApi.class);
      HashMap<String, String> params = getQueryParams();
      params.put("section", section);
      params.put("page", pageValue);
      return storyApi.getStories(params)
          .flatMap(storyResponse -> Single.just(storyResponse.getStoryList()));
  }

  private HashMap<String, String> getQueryParams() {
    HashMap<String, String> paramsMap = new HashMap<>();
    paramsMap.put("api-key", "c57a4e0a-f691-4b80-b9b2-08a7376a2a10");
    paramsMap.put("show-fields", "thumbnail,trailText");
    paramsMap.put("show-tags", "contributor");
    paramsMap.put("page-size", prefsUtils.getPageSize());
    paramsMap.put("order-by", prefsUtils.getOrderBy());
    return paramsMap;
  }
}
