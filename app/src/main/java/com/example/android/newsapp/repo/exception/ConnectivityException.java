package com.example.android.newsapp.repo.exception;

/**
 * Marker Exception raised when there is something wrong on the network.
 */
public class ConnectivityException extends Throwable {

  public ConnectivityException() {
    super("No Internet Connection");
  }
}
