package com.example.android.newsapp.repo.model;

import com.example.android.newsapp.model.Story;
import com.google.gson.annotations.SerializedName;
import java.util.Collections;
import java.util.List;

public class StoryResponseWrapper {

  @SerializedName("response")
  private StoryResponse storyResponse;

  public List<Story> getStoryList() {
    return storyResponse != null ? storyResponse.storyList : Collections.emptyList();
  }

  private class StoryResponse {

    @SerializedName("results")
    private List<Story> storyList;
  }
}
