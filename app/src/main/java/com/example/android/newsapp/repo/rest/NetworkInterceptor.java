package com.example.android.newsapp.repo.rest;

import com.example.android.newsapp.utils.NetworkUtils;
import java.io.IOException;
import java.net.ConnectException;
import javax.inject.Inject;
import javax.inject.Singleton;
import okhttp3.Interceptor;
import okhttp3.Response;

@Singleton
public class NetworkInterceptor implements Interceptor {

  private NetworkUtils networkUtils;

  @Inject
  public NetworkInterceptor(NetworkUtils networkUtils) {
    this.networkUtils = networkUtils;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    if (!networkUtils.isNetworkAvailable()) {
      throw new ConnectException();
    }
    return chain.proceed(chain.request());
  }
}
