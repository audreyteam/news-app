package com.example.android.newsapp.repo.rest;

import javax.inject.Singleton;
import retrofit2.Retrofit;

@Singleton
public class RestClient {

  private final Retrofit retrofit;

  public RestClient(Retrofit retrofit) {
    this.retrofit = retrofit;
  }

  public StoryApi getApiClient(Class<StoryApi> apiClass) {
    return retrofit.create(apiClass);
  }
}
