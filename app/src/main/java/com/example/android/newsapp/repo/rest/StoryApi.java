package com.example.android.newsapp.repo.rest;

import com.example.android.newsapp.repo.model.StoryResponseWrapper;
import io.reactivex.Single;
import java.util.Map;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface StoryApi {

  @GET("search")
  Single<StoryResponseWrapper> getStories(@QueryMap Map<String, String> params);
}
