package com.example.android.newsapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import com.example.android.newsapp.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Utils for date formatting.
 */
public class DateUtils {

  private static final String TAG = "DateUtils";
  private static final int MAX_DAYS = 31;
  private static final int MAX_MONTHS = 12;

  private DateUtils() {
    // Can't be constructed outside this class.
  }

  /**
   * Return a human readable formatted time from a long value.
   * @param elapsedTimeInMs the elapsed time in milliseconds.
   *
   * @return a human readable time in {@link String} format.
   */
  public static String toReadableTime(Context context, long elapsedTimeInMs) {
    Resources res = context.getResources();
    long timeInMins = TimeUnit.MILLISECONDS.toMinutes(elapsedTimeInMs);
    long timeInHours = TimeUnit.MILLISECONDS.toHours(elapsedTimeInMs);
    long timeInDays = TimeUnit.MILLISECONDS.toDays(elapsedTimeInMs);
    long timeInMonths = timeInDays / MAX_DAYS;
    long timeInYears = timeInMonths / MAX_MONTHS;
    String readableTime;
    if (timeInYears > 0) {
      readableTime = res
          .getQuantityString(R.plurals.year_text, (int) timeInYears, (int) timeInYears);
    } else if (timeInMonths > 0) {
      readableTime = res
          .getQuantityString(R.plurals.month_text, (int) timeInMonths, (int) timeInMonths);
    } else if (timeInDays > 0) {
      readableTime = res.getQuantityString(R.plurals.day_text, (int) timeInDays, (int) timeInDays);
    } else if (timeInHours > 0) {
      readableTime = res
          .getQuantityString(R.plurals.hour_text, (int) timeInHours, (int) timeInHours);
    } else {
      readableTime = res
          .getQuantityString(R.plurals.minute_text, (int) timeInMins, (int) timeInMins);
    }
    return readableTime;
  }

  /**
   * Converts a String date in Milliseconds.
   * @param dateToConvert the date to be converted.
   *
   * @return a long that represent dateToConvert in milliseconds.
   */
  public static long convertDateToMs(String dateToConvert) {
    if (TextUtils.isEmpty(dateToConvert)) {
      return 0;
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    long convertedDate = 0;
    try {
      Date date = dateFormat.parse(dateToConvert);
      if (date != null) {
        convertedDate = date.getTime();
      }
    } catch (ParseException e) {
      Log.e(TAG, "Problem parsing the date", e);
    }
    return convertedDate;
  }

}
