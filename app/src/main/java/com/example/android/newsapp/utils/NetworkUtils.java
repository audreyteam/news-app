package com.example.android.newsapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.android.newsapp.di.annotation.ApplicationContext;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class NetworkUtils {

  private final Context applicationContext;

  @Inject
  public NetworkUtils(@ApplicationContext Context applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * Returns Whether the network is available.
   */
  public boolean isNetworkAvailable() {
    ConnectivityManager cm =
        (ConnectivityManager) applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    boolean isConnected = activeNetwork != null &&
        activeNetwork.isConnected();
    return isConnected;
  }

}
