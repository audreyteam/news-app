package com.example.android.newsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.example.android.newsapp.R;
import com.example.android.newsapp.di.annotation.ApplicationContext;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PrefsUtils {

  private static final String APP_PREFERENCES_FILE_KEY = "app_preferences_file_key";
  private static final String FIRST_TIME_KEY = "is_first_time";
  private static final String CATEGORY_SELECTED_KEY = "category_selected_names_key";

  private final Context applicationContext;

  @Inject
  public PrefsUtils(@ApplicationContext Context applicationContext) {
    this.applicationContext = applicationContext;
  }

  private SharedPreferences getPreferences() {
    return applicationContext.getSharedPreferences(APP_PREFERENCES_FILE_KEY, Context.MODE_PRIVATE);
  }

  /**
   * Checks the user first time when the app is started.
   */
  public boolean isFirstTimeUser() {
    return getPreferences().getBoolean(FIRST_TIME_KEY, true);
  }

  /**
   * Sets the user first time to false.
   */
  public void putFirstTimeUser() {
    SharedPreferences.Editor editor = getPreferences().edit();
    editor.putBoolean(FIRST_TIME_KEY, false);
    editor.apply();
  }

  /**
   * Saves the selected categories in the preferences so that the user knows what he chose for the
   * news.
   */
  public void putSelectedCategories(Set<String> selectedCategories) {
    SharedPreferences.Editor editor = getPreferences().edit();
    editor.putStringSet(CATEGORY_SELECTED_KEY, selectedCategories);
    editor.apply();
  }

  /**
   * Retrieves the selected categories.
   */
  public Set<String> getSelectedCategories() {
    return getPreferences().getStringSet(CATEGORY_SELECTED_KEY, new HashSet<String>());
  }

  /**
   * Retrieves page size wanted by the user in Settings
   */
  public String getPageSize() {
    SharedPreferences sharedPrefs = PreferenceManager
        .getDefaultSharedPreferences(applicationContext);
    return sharedPrefs.getString(applicationContext.getString(R.string.settings_page_size_key),
        applicationContext.getString(R.string.settings_page_size_default));
  }

  /**
   * Retrieves the news order chosen by the user in Settings
   */
  public String getOrderBy() {
    SharedPreferences sharedPrefs = PreferenceManager
        .getDefaultSharedPreferences(applicationContext);
    return sharedPrefs.getString(applicationContext.getString(R.string.settings_order_by_key),
        applicationContext.getString(R.string.settings_order_by_default));
  }
}
