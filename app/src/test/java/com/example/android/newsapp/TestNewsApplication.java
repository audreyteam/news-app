package com.example.android.newsapp;

import com.example.android.newsapp.di.AppModule;
import com.example.android.newsapp.di.DaggerFakeNewsComponent;
import com.example.android.newsapp.di.FakeNetworkModule;
import com.example.android.newsapp.di.FakeNewsComponent;

public class TestNewsApplication extends NewsApplication {

  private FakeNewsComponent component;

  @Override
  public void onCreate() {
    super.onCreate();
    component = DaggerFakeNewsComponent.builder().appModule(new AppModule(this))
        .networkModule(new FakeNetworkModule(Config.BASE_URL)).build();
    component.inject(this);
  }

  public FakeNewsComponent getComponent() {
    return component;
  }
}
