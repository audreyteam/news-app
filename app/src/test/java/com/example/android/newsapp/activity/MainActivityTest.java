package com.example.android.newsapp.activity;

import static com.example.android.newsapp.activity.MainActivity.EXTRA_SHOULD_NOT_REFRESH_CONTENT;

import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.example.android.newsapp.R;
import com.example.android.newsapp.TestNewsApplication;
import com.example.android.newsapp.fragment.StoryFragment;
import com.example.android.newsapp.utils.PrefsUtils;
import com.google.common.truth.Truth;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;

@RunWith(RobolectricTestRunner.class)
@Config(application = TestNewsApplication.class)
public class MainActivityTest {

  private static final String[] CATEGORIES = {"Technology", "Science", "News"};
  private static final String NAV_LIST_VIEW_STATE_KEY = "nav_list_view_state_key";
  private ActivityController<MainActivity> activityController;
  private MainActivity activity;
  private ListView navDrawerListView;
  private DrawerLayout drawerLayout;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    TestNewsApplication application = (TestNewsApplication) RuntimeEnvironment.application;
    PrefsUtils prefsUtils = application.getComponent().getPrefsUtils();
    prefsUtils.putSelectedCategories(new HashSet<>(Arrays.asList(CATEGORIES)));
    activityController = Robolectric.buildActivity(MainActivity.class).setup();
    activity = activityController.get();
    navDrawerListView = activity.navDrawerListView;
    drawerLayout = activity.drawerLayout;
//    Robolectric.getForegroundThreadScheduler().pause();
  }

  @Test
  public void testStoryFragmentVisible_whenStart() {
    Fragment fragment = activity.getSupportFragmentManager().findFragmentById(R.id.frame_layout);
    Truth.assertThat(fragment).isNotNull();
    Truth.assertThat(fragment.getClass()).isEqualTo(StoryFragment.class);
    Truth.assertThat(fragment.isVisible()).isTrue();
    Truth.assertThat(navDrawerListView.isItemChecked(0)).isTrue();
    Truth.assertThat(navDrawerListView.getAdapter().getItem(0).toString())
        .isEqualTo(activity.getTitle().toString());
  }

  @Test
  public void testStoryFragmentVisible_whenConfigChange() {
    Configuration configuration = new Configuration();
    configuration.orientation = Configuration.ORIENTATION_LANDSCAPE;
    activity = activityController.configurationChange(configuration).get();
    Fragment fragment = activity.getSupportFragmentManager().findFragmentById(R.id.frame_layout);
    Truth.assertThat(fragment).isNotNull();
    Truth.assertThat(fragment.getClass()).isEqualTo(StoryFragment.class);
    Truth.assertThat(fragment.getView().getVisibility()).isEqualTo(View.VISIBLE);
  }

  @Test
  public void testClickNavDrawerItem_changeNewsTopic() {
    drawerLayout.openDrawer(Gravity.START, false);
    Truth.assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isTrue();
    String currentTitle = navDrawerListView.getAdapter().getItem(0).toString();
    Shadows.shadowOf(navDrawerListView).performItemClick(1);
    Truth.assertThat(navDrawerListView.isItemChecked(1)).isTrue();
    Truth.assertThat(currentTitle).isNotEqualTo(activity.getTitle().toString());
  }

  @Test
  public void testClickOnHomeButton() {
    RoboMenuItem homeMenuItem = new RoboMenuItem(android.R.id.home);
    activity.onOptionsItemSelected(homeMenuItem);
//    Truth.assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isTrue();
  }

  @Test
  public void testClickSettingsButton() {
    MenuItem settingsMenuItem = new RoboMenuItem(R.id.menu_settings);
    activity.onOptionsItemSelected(settingsMenuItem);
    Intent nextIntent = Shadows.shadowOf(activity).getNextStartedActivity();
    Truth.assertThat(nextIntent.getComponent())
        .isEqualTo(new ComponentName(activity, SettingsActivity.class));
  }

  @Test
  public void testClickOnBackButton_closeNavDrawer() {
    drawerLayout.openDrawer(GravityCompat.START, false);
    Truth.assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isTrue();
    activity.onBackPressed();
//    Truth.assertThat(drawerLayout.isDrawerOpen(GravityCompat.START)).isFalse();
  }

  @Test
  public void testClickOnBackButton_navigateBack() {
    activity.onBackPressed();
    Truth.assertThat(activity.isFinishing()).isTrue();
  }

  @Test
  public void testCallOnSaveInstanceState_saveListState() {
    Bundle bundle = new Bundle();
    activity.onSaveInstanceState(bundle);
    Truth.assertThat(bundle.containsKey(NAV_LIST_VIEW_STATE_KEY)).isTrue();
  }

  @Test
  public void testOnNewIntent_refreshNews() {
    Intent intent = new Intent();
    activity.onNewIntent(intent);
    Truth.assertThat(!intent.hasExtra(EXTRA_SHOULD_NOT_REFRESH_CONTENT)).isTrue();
  }

  @Test
  public void testOnNewIntent_doNotRefreshNews() {
    Intent intent = new Intent();
    intent.putExtra(EXTRA_SHOULD_NOT_REFRESH_CONTENT, true);
    activity.onNewIntent(intent);
    Truth.assertThat(intent.hasExtra(EXTRA_SHOULD_NOT_REFRESH_CONTENT)).isTrue();
  }

}