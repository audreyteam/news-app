package com.example.android.newsapp.di;

import com.example.android.newsapp.repo.rest.NetworkInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.mockito.Mockito;

public class FakeNetworkModule extends NetworkModule {

  public FakeNetworkModule(String url) {
    super(url);
  }

  @Override
  public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor,
      NetworkInterceptor networkInterceptor) {
    return Mockito.mock(OkHttpClient.class);
  }
}
