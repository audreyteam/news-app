package com.example.android.newsapp.di;

import com.example.android.newsapp.utils.PrefsUtils;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import javax.inject.Singleton;

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationBinders.class, AppModule.class,
    NetworkModule.class})
public interface FakeNewsComponent extends NewsComponent {
  PrefsUtils getPrefsUtils();
}
