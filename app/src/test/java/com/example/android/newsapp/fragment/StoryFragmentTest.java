package com.example.android.newsapp.fragment;

import static org.robolectric.Shadows.shadowOf;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.example.android.newsapp.R;
import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.model.Story.TagEditor;
import com.google.common.truth.Truth;
import dagger.android.AndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowPackageManager;
import org.robolectric.shadows.ShadowToast;

@RunWith(RobolectricTestRunner.class)
public class StoryFragmentTest {

  private static final String FAKE_WEB_URL = "www.home.org";
  private static final int STORY_COUNT = 8;

  @Mock
  private static StoryPresenter presenter;
  @InjectMocks
  private StoryFragment fragment;
  private StoryActivityTest activityTest;
  private TextView emptyView;
  private RecyclerView recyclerView;
  private SwipeRefreshLayout refreshLayout;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    activityTest = Robolectric.buildActivity(StoryActivityTest.class).setup().get();
    fragment = (StoryFragment) activityTest.getSupportFragmentManager()
        .findFragmentById(android.R.id.content);
    fragment.storyPresenter = presenter;
    emptyView = fragment.emptyView;
    recyclerView = fragment.recyclerView;
    refreshLayout = fragment.refreshLayout;
  }

  @Test
  public void testStoryFragmentVisible_whenStart() {
    Truth.assertThat(fragment).isNotNull();
    Truth.assertThat(fragment.isVisible()).isTrue();
  }

  @Test
  public void testRefreshNews() {
    fragment.onRefresh();
    Mockito.verify(presenter).refreshStories(Mockito.anyString());
  }

  private String getActivityTitle() {
    return activityTest.getTitle().toString();
  }

  @Test
  public void testLoadMoreNews() {
    fragment.onLoadMoreStories();
    Mockito.verify(presenter).loadMoreStories(getActivityTitle());
  }

  @Test
  public void testClickStoryItem_launchWebsite() {
    fragment.onStoryItemClick(new Story("News", "fragrance", FAKE_WEB_URL));
    Mockito.verify(presenter).launchWebsite(FAKE_WEB_URL);
  }

  @Test
  public void testClickEditor_launchWebsite() {
    fragment.onEditorClick(new TagEditor("Paul", FAKE_WEB_URL, ""));
    Mockito.verify(presenter).launchWebsite(FAKE_WEB_URL);
  }

  @Test
  public void testChangeSectionName() {
    fragment.notifySectionNameChanged();
    Mockito.verify(presenter).changeSectionName(getActivityTitle());
  }

  @Test
  public void testCallOnDestroyView() {
    fragment.onDestroyView();
    Mockito.verify(presenter).onDestroy();
  }

  private ArrayList<Story> getStories() {
    ArrayList<Story> stories = new ArrayList<>();
    for (int i = 0; i < STORY_COUNT; i++) {
      stories.add(new Story("News", "fragrance", FAKE_WEB_URL));
    }
    return stories;
  }

  private void verifyEmptyViewIsShown() {
    Truth.assertThat(emptyView.getVisibility()).isEqualTo(View.VISIBLE);
    Truth.assertThat(emptyView.getText())
        .isEqualTo(activityTest.getString(R.string.no_data));
  }

  @Test
  public void testDisplayNews_loadNews_WhenStoryListExists() {
    fragment.displayNews(getStories(), false);
    Truth.assertThat(recyclerView.getAdapter().getItemCount() - 1)
        .isEqualTo(STORY_COUNT);
    Truth.assertThat(emptyView.getVisibility()).isEqualTo(View.GONE);
  }

  @Test
  public void testDisplayNews_loadNews_WhenStoryListNotExists() {
    fragment.displayNews(null, false);
    verifyEmptyViewIsShown();
  }

  @Test
  public void testDisplayNews_loadMoreNews_WhenStoryListExists() {
    fragment.displayNews(getStories(), true);
    fragment.displayNews(getStories(), true);
    Truth.assertThat(recyclerView.getAdapter().getItemCount() - 1)
        .isEqualTo(2 * STORY_COUNT);
    Truth.assertThat(emptyView.getVisibility()).isEqualTo(View.GONE);
  }

  @Test
  public void testDisplayNews_loadMoreNews_WhenStoryListNotExists() {
    fragment.displayNews(null, true);
    verifyEmptyViewIsShown();
  }

  @Test
  public void testShowNoConnectionError() {
    fragment.showNoConnectionError();
    Truth.assertThat(emptyView.getVisibility()).isEqualTo(View.VISIBLE);
    Truth.assertThat(emptyView.getText())
        .isEqualTo(activityTest.getString(R.string.no_connection));
  }

  @Test
  public void testScrollRecyclerViewToBeginning() {
    fragment.displayNews(getStories(), true);
    fragment.displayNews(getStories(), true);
    fragment.displayNews(getStories(), true);
    recyclerView.scrollToPosition(10);
    fragment.scrollStoriesToBeginning();
    int initialPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
        .findFirstVisibleItemPosition();
    Truth.assertThat(initialPosition).isEqualTo(0);
  }

  @Test
  public void testRefreshingState_isRefreshing() {
    fragment.setRefreshingState(true);
    Truth.assertThat(refreshLayout.isRefreshing()).isTrue();
  }

  @Test
  public void testRefreshingState_isNotRefreshing() {
    fragment.setRefreshingState(false);
    Truth.assertThat(refreshLayout.isRefreshing()).isFalse();
  }

  @Test
  public void testShowNoConnectionToast() {
    fragment.showNoConnectionToast();
    Truth.assertThat(ShadowToast.getLatestToast().getDuration()).isEqualTo(Toast.LENGTH_SHORT);
    Truth.assertThat(ShadowToast.getTextOfLatestToast())
        .isEqualTo(activityTest.getString(R.string.no_connection));
  }

  @Test
  public void testLaunchWebsite() {
    Intent expectedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FAKE_WEB_URL));
    setupPackageManager(expectedIntent);
    fragment.launchWebsite(FAKE_WEB_URL);
    Intent nextIntent = Shadows.shadowOf(activityTest).getNextStartedActivity();
    Truth.assertThat(nextIntent.getAction()).isEqualTo(expectedIntent.getAction());
    Truth.assertThat(nextIntent.getData()).isEqualTo(expectedIntent.getData());
  }

  private void setupPackageManager(Intent expectedIntent) {
    ShadowPackageManager packageManager = shadowOf(
        RuntimeEnvironment.application.getPackageManager());
    ResolveInfo resolveInfo = new ResolveInfo();
    ActivityInfo activityInfo = new ActivityInfo();
    activityInfo.applicationInfo = new ApplicationInfo();
    activityInfo.applicationInfo.packageName = "com.example.android.newsapp";
    activityInfo.name = "activity";
    resolveInfo.activityInfo = activityInfo;
    resolveInfo.isDefault = true;
    packageManager.addResolveInfoForIntent(expectedIntent, resolveInfo);
  }

  @Test
  public void testFetchStories_whenFragmentStart() {
    Mockito.verify(presenter).fetchStories(Mockito.anyString());
  }

  private static class StoryActivityTest extends AppCompatActivity implements
      HasSupportFragmentInjector {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      StoryFragment fragment = new StoryFragment();
      fragment.storyPresenter = presenter;
      getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment)
          .commit();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
      return Mockito.mock(AndroidInjector.class);
    }


  }
}