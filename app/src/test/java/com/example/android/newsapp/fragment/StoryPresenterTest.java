package com.example.android.newsapp.fragment;

import static org.mockito.ArgumentMatchers.anyString;

import com.example.android.newsapp.fragment.StoryPresenter.StoryView;
import com.example.android.newsapp.model.Story;
import com.example.android.newsapp.repo.StoryRepository;
import com.example.android.newsapp.repo.exception.ConnectivityException;
import com.example.android.newsapp.utils.SchedulerProvider;
import com.google.common.truth.Truth;
import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;
import java.util.ArrayList;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class StoryPresenterTest {

  private static final String FAKE_WEB_URL = "www.home.org";
  private static final int STORY_COUNT = 4;

  private StoryPresenter presenter;
  @Mock
  private StoryView storyView;
  @Mock
  private StoryRepository storyRepository;
  @Mock
  private SchedulerProvider schedulerProvider;
  private TestScheduler testScheduler;

  @Before

  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    presenter = new StoryPresenter(storyView, storyRepository, schedulerProvider);
    testScheduler = new TestScheduler();
    Mockito.when(schedulerProvider.io()).thenReturn(testScheduler);
    Mockito.when(schedulerProvider.mainThread()).thenReturn(testScheduler);
  }

  private ArrayList<Story> getStories() {
    ArrayList<Story> stories = new ArrayList<>();
    for (int i = 0; i < STORY_COUNT; i++) {
      stories.add(new Story("News", "fragrance", FAKE_WEB_URL));
    }
    return stories;
  }

  @Test
  public void testRefreshStories_onSuccess() {
    ArrayList<Story> storyList = getStories();
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.just(storyList));
    presenter.refreshStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).scrollStoriesToBeginning();
    Mockito.verify(storyView).displayNews(storyList, false);
  }

  @Test
  public void testRefreshStories_onError() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new ConnectivityException()));
    presenter.refreshStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).showNoConnectionToast();
  }

  @Test
  public void testLoadMoreStories_onSuccess() {
    ArrayList<Story> storyList = getStories();
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.just(storyList));
    presenter.loadMoreStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).displayNews(storyList, true);
  }

  @Test
  public void testLoadMoreStories_onError() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new ConnectivityException()));
    presenter.loadMoreStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).showNoConnectionToast();
  }

  @Test
  public void testFetchStories_onError_noConnection() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new ConnectivityException()));
    presenter.fetchStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).showNoConnectionError();
  }

  @Test
  public void testFetchStories_onError_other() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new Throwable()));
    presenter.fetchStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).displayNews(Collections.emptyList(), false);
  }

  @Test
  public void testFetchStories_onSuccess() {
    ArrayList<Story> storyList = getStories();
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.just(storyList));
    presenter.fetchStories("News");
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).displayNews(storyList, false);
  }

  @Test
  public void testChangeSectionName_onSuccess() {
    ArrayList<Story> storyList = getStories();
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.just(storyList));
    presenter.changeSectionName("News");
    Mockito.verify(storyView).scrollStoriesToBeginning();
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).displayNews(storyList, false);
  }

  @Test
  public void testChangeSectionName_onError_noConnection() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new ConnectivityException()));
    presenter.changeSectionName("News");
    Mockito.verify(storyView).scrollStoriesToBeginning();
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).setRefreshingState(false);
    Mockito.verify(storyView).showNoConnectionError();
  }

  @Test
  public void testChangeSectionName_onError_other() {
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.error(new Throwable()));
    presenter.changeSectionName("News");
    Mockito.verify(storyView).scrollStoriesToBeginning();
    Mockito.verify(storyView).setRefreshingState(true);
    testScheduler.triggerActions();
    Mockito.verify(storyView).displayNews(Collections.emptyList(), false);
  }

  @Test
  public void testOnDestroy() {
    ArrayList<Story> storyList = getStories();
    Mockito.when(storyRepository.getStoryList(anyString(), anyString()))
        .thenReturn(Single.just(storyList));
    presenter.fetchStories("News");
    testScheduler.triggerActions();
    Truth.assertThat(presenter.getCompositeDisposable().size()).isNotEqualTo(0);
    presenter.onDestroy();
    Truth.assertThat(presenter.getCompositeDisposable().size()).isEqualTo(0);
  }

  @Test
  public void testLaunchWebsite() {
    presenter.launchWebsite(FAKE_WEB_URL);
    Mockito.verify(storyView).launchWebsite(FAKE_WEB_URL);
  }
}